<!DOCTYPE html>
<html lang="en">
<head>
    @include ('template.template-user.meta')
</head>
<body>
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WRJ98L4"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<div id="load-page">
<div class="lds-ellipsis">
    <label>Loading</label>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
</div>
</div>
<div id="url" style="display: none">{{url('')}}</div>
{{ csrf_field() }}
<div class="container-fluid main-container">
    <div class="col-md-12 content">
        <div class="panel panel-default">
            <div class="panel-heading col-md-12">
                <div class="col-md-6"><h5>โรงเรียนอวกาศ</h5></div><div class="text-right col-md-6">
                    <button style="display: {{(Request::is('joint-activity') ? 'none':'')}}" data-title="Search" data-toggle="modal" data-target="#search" class="btn btn-warning">ค้นหา</button>
                    @if(Request::is('name-in-activity/*') || Request::is('joint-activity'))
                        <a href="{{url('')}}" class="btn btn-success">ย้อนหลับ</a>
                        <a class="btn btn-danger print" onclick="window.print();"><span style="padding: 2px;" class="glyphicon glyphicon-print"></span></a>
                    @endif
                </div>
            </div>
            <div class="panel-body">
                @yield('content')
            </div>
        </div>
    </div>
    @include ('template.template-user.footer')
</div>
<script type="text/javascript" src="{{url('/js/javascript.js')}}"></script>
<script>
    setTimeout(function () {
        $('#load-page').css('display','none');
    },500)
</script>
</body>
</html>
