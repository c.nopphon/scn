@extends('template.template-user.template')

@section('title', 'ตารางกิจกรรม')
@section('content')
    <div class="container text-center">
        <img src="{{url('img/logo.png')}}" style="width: 100px">
    </div>
    <style>
        img.barcode{
            height: 63px
        }
        img.qrbarcode{
            height: 70px
        }
        .boxbarcode{
            text-align: right;
        }

        @media print {
            body{
                margin: 0px;
                padding: 0px;
            }
            a.btn.btn-success,#addActivity_paginate,#addActivity_filter,#addActivity_length,a.print{display: none}
            .boxbarcode{
                text-align: left;

            }

        }
        @media  (max-width: 992px){
            .boxbarcode{
                text-align: left ;
                margin-bottom: 30px;
            }
        }    </style>
    @if($warning == 'danger')
        <div class="clearfix"> </div>
        <br>
        <div class="alert alert-{{$warning}}" role="alert">
            {{$message}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @else
    <link href="{{url('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <div class="clearfix"></div><br>
    <div class="col-sm-6" align="left">
        <h4> ชื่อ : <label>{{$name->PREFIXNAME.$name->STUDENTNAME.' '.$name->STUDENTSURNAME}} [ {{$name->STUDENTCODE}} ]</label></h4>
        <h4>แต้มสะสม : <label style="color: red">{{$sumScore}}</label> </h4>
    </div>
    <div class="col-sm-6 boxbarcode">
        <img class="qrbarcode" src='https://barcode.tec-it.com/barcode.ashx?data={{$name->STUDENTCODE}}&code=MobileQRCode&dpi=96' alt='{{$name->STUDENTCODE}}'/>
        &nbsp;&nbsp;
        <img  class="barcode" src='https://barcode.tec-it.com/barcode.ashx?data={{$name->STUDENTCODE}}&code=Code128&dpi=200' alt='{{$name->STUDENTCODE}}'/>
    </div>
    <div class="clearfix"></div>

    <table id="addActivity" class="table table-striped table-bordered" style="width:100%;font-size: 12px">
        <thead>
        <tr>
            <th>กิจกรรม</th>
            <th>เวลา</th>
            <th>แต้ม</th>
        </tr>
        </thead>
        <tbody>
        @foreach($activity as $index => $val)
            <tr>
                <td>{{$val->name_activity}}</td>
                <td>{{$val->DATE}}</td>
                <td>{{$val->score}}</td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <th>กิจกรรม</th>
            <th>เวลา</th>
            <th>แต้ม</th>
        </tr>
        </tfoot>
    </table>
<script>
    $(document).ready(function () {
        $('#addActivity').DataTable({
            pageLength: 100,
            "order": [[2, "desc"]]
        })
    });
</script>
<script type="text/javascript" src="{{url('/js/javascript.js')}}"></script>

    @endif
@endsection

