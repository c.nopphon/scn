@extends('template.template')

@section('title', 'เพิ่มกิจกรรม')
@section('content')
    @if(session('warning'))
        <div class="alert alert-{{session('warning')}}" role="alert">
            {{session('message')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="container">
        <div class="row">
            <h2></h2>
        </div>


    <form class="form-horizontal" action="{{action('AddActivityControllers@AddActivity')}}" method="post">
        <fieldset>
            <!-- Form Name -->
            <legend>เพิ่มกิจกรรม</legend>
            <!-- Text input-->
            <div class="control-group col-md-4">
                <label class="control-label" for="Username">ชื่อกิจกรรม :</label>
                <div class="controls">
                    <input name="name" type="text" class="form-control" required autocomplete="no">
                </div>
            </div>
            <div class="control-group col-md-4">
                <label class="control-label" for="Username">ผู้เข้าร่วม : </label>
                <div class="controls">
                    <select class="form-control" name="nst_class" required="">
                        <option value="0">ทั้งหมด</option>
                        @for ($i = 1; $i <= 8; $i++)
                            <option value="{{ $i }}">ปี {{ $i }}</option>
                        @endfor
{{--                        <option disabled>-- มัธยมศึกษา --</option>--}}
{{--                        @for ($i = 1; $i <= 6; $i++)--}}
{{--                            <option value="{{ $i }}">ม.{{ $i }}</option>--}}
{{--                        @endfor--}}

                    </select>
                </div>
            </div>
            <div class="control-group col-md-4">
                <label class="control-label" for="Username">แต้ม : </label>
                <div class="controls">
                    <input name="score" type="number" class="form-control" required autocomplete="no" value="1">
                </div>
            </div>
            <div class="control-group col-md-4">
                <label class="control-label" for="Username">เริ่ม : </label>
                <div class="controls">
                    <input name="start" type="datetime-local" class="form-control" required autocomplete="no" value="<?php echo date('Y-m-d\TH:i')?>">
                </div>
            </div>
            <div class="control-group col-md-4">
                <label class="control-label" for="Username">สิ้นสุด : </label>
                <div class="controls">
                    <input  name="end" type="datetime-local" placeholder="" class="form-control" required autocomplete="no" value="<?php echo date('Y-m-d\TH:i')?>">

                </div>
            </div>
            <!-- Password input-->
            <div class="control-group col-md-4">
                <label class="control-label" for="Password">หมายเหตุ : </label>
                <div class="controls">
                    <input  name="remark" type="text" placeholder="" class="form-control">
                    {{ csrf_field() }}
                </div>
            </div>
            <!-- Button -->
            <div class="control-group col-md-12 text-right">
                <label class="control-label" for="singlebutton"></label>
                <div class="controls">
                    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Save</button>
                    <button type="reset" id="reset" name="reset" class="btn btn-default">Reset</button>
                </div>
            </div>
        </fieldset>
    </form>
    </div>

@endsection