<!DOCTYPE html>
<html lang="en">
<head>
    @include ('admin.meta')
</head>
<body>
<div id="load-page">
<div class="lds-ellipsis">
    <label>Loading</label>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
</div>
</div>
@include ('admin.head')
<div id="url" style="display: none">{{url('')}}</div>
{{ csrf_field() }}
<div class="container-fluid main-container">
    <div class="col-md-2 sidebar">
        <div class="row">
            <!-- uncomment code for absolute positioning tweek see top comment in css -->
            <div class="absolute-wrapper"> </div>
            <!-- Menu -->
            @include ('admin.menu')
        </div>  		</div>
    <div class="col-md-10 content">
        <div class="panel panel-default">
            <div class="panel-heading">
                โรงเรียนอวกาศ
            </div>
            <div class="panel-body">
                @yield('content')
            </div>
        </div>
    </div>
    @include ('admin.footer')
</div>
<script type="text/javascript" src="{{url('/js/javascript.js')}}"></script>
<script>
    setTimeout(function () {
        $('#load-page').css('display','none');
    },500)
</script>
</body>
</html>
