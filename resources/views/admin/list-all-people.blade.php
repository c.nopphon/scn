@extends('template.template')

@section('title', 'รายชื่อทั้งหมด')
@section('content')
    @if(session('warning'))
        <div class="alert alert-{{session('warning')}}" role="alert">
            {{session('message')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <style>
        .STUDENTCODE {
            border: none;
            background-color: #fff0;
            box-shadow: none;
        }
    </style>
    <div id="updateStudentCode" class="alert alert-success" role="alert" style="display:none;">
        <label class="message"></label>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <link href="{{url('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>รหัสนักศึกษา</th>
            <th>ชื่อ - สกุล</th>
            <th>หลักสูตร</th>
            <th>#</th>
        </tr>
        </thead>
        <tbody>
        @foreach($listAllPeople as $index => $val)
            <tr>
                <td>
                    <input onchange="updateStudentCode('{{$val->ID}}',this.value);" class="form-control STUDENTCODE" type="number" value="{{$val->STUDENTCODE}}">
                    <p style="display:none;"> {{$val->STUDENTCODE}}</p>
                </td>

                <td>{{$val->PREFIXNAME}}{{$val->STUDENTNAME}} {{$val->STUDENTSURNAME}}</td>
                <td>
                    {{$val->LEVELNAME}}
                    <br>
                    สาขา : {{$val->PROGRAMNAME}}[{{$val->ADMITACADYEAR}}]
                </td>
                <td class="text-center">
                    <a class="btn btn-info btn-xs"
                       href="javascript:MM_openBrWindow('{{url('admin/list-all-people/'.$val->ID)}}','createQrCode','scrollbars=no,width=400,height=500')"><span
                                class="glyphicon glyphicon-qrcode"></span></a>
                    <a data-placement="top" onmouseover="getMessage({{$val->ID}})" data-toggle="tooltip" title="Edit">
                        <button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal"
                                data-target="#edit"><span class="glyphicon glyphicon-pencil"></span></button>
                    </a>
                    <a onclick="document.getElementById('idDelete').value = '{{$val->ID}}'" data-title="Delete"
                       data-toggle="modal" data-target="#delete" href="" class="btn btn-danger btn-xs"><span
                                class="glyphicon glyphicon-trash"></span></a>
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <th>รหัสนักศึกษา</th>
            <th>ชื่อ - สกุล</th>
            <th>หลักสูตร</th>
            <th>#</th>
        </tr>
        </tfoot>
    </table>
    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <input name="id" type="hidden" class="form-control">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span
                                class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                    <h4 class="modal-title custom_align" id="Heading">Edit Your Detail</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group col-md-12 updatelist" style="display: none">
                        <div class="alert alert-success" role="alert">
                            อัพเดทข้อมูลสำเร็จ
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    <div class="form-group  col-md-6">
                        <label class="control-label" for="Username">รหัสนักศึกษา :</label>
                        <div class="controls">
                            <input name="student_id" type="number" class="form-control" required autocomplete="no">
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label" for="Username">หลักศูตร : </label>
                        <div class="controls">
                            <select class="form-control" name="level" required>
                                @foreach ($listLevel as $index => $val)
                                    <option value="{{$val->LEVELID}}">{{$val->LEVELNAME}}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label" for="Username">สาขา : </label>
                        <div class="controls">
                            <select class="form-control" name="program" required>
                                <option value="" selected>เลือก</option>
                                @foreach ($listProgram as $index => $val)
                                    <option value="{{$val->PROGRAMID}}">{{$val->PROGRAMNAME}}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label" for="Username">ปีการศึกษา : </label>
                        <div class="controls">
                            <select class="form-control" name="admitacadyear" required>
                                @foreach ($admitacadyear as $index)
                                    <option value="{{$index}}">{{$index}}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label" for="Username">คำนำหน้า : </label>
                        <div class="controls">
                            <select class="form-control" name="prefix">
                                @foreach ($listPrefix as $index => $val)
                                    <option value="{{$val->PREFIXID}}">{{$val->PREFIXNAME}}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label" for="fname">ชื่อ : </label>
                        <div class="controls">
                            <input name="fname" type="text" placeholder="" class="form-control" required
                                   autocomplete="no">

                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label" for="lname">สกุล : </label>
                        <div class="controls">
                            <input name="lname" type="text" placeholder="" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer ">
                    <button type="submit" class="btn btn-warning btn-lg" style="width: 100%;"
                            onclick="updateName();"><span
                                class="glyphicon glyphicon-ok-sign"></span> Update
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{action('ListAllPeopleControllers@deleteName')}}" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span
                                    class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                        <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
                        <input type="hidden" name="idDelete" id="idDelete">
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-danger">
                            <h4><span class="glyphicon glyphicon-warning-sign"></span> คำเตือน!!</h4>
                            <p>- รายชื่อที่เคยเข้าร่วมกิจกรรมก็จะถูกลบไปด้วย</p>
                        </div>


                    </div>
                    <div class="modal-footer ">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok-sign"></span> ยืนยันการลบ
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><span
                                    class="glyphicon glyphicon-remove"></span> ยกเลิก
                        </button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <script>
        function updateStudentCode(id, student_id) {
            $.ajax({
                type: 'POST',
                url: '{{action('AddListControllers@updateStudentCode')}}',
                data: {
                    _token: $("[name='_token']").val(),
                    id: id,
                    student_id: student_id,
                },
                success: function (data) {
                    $('#updateStudentCode').removeClass('alert alert-danger');
                    $('#updateStudentCode').addClass('alert alert-' + data.warning);
                    $('#updateStudentCode').css('display', 'block');
                    $('#updateStudentCode .message').text(data.message+' : '+data.codeStu)

                }, error: function (error) {
                    $('#updateStudentCode').removeClass('alert alert-success');
                    $('#updateStudentCode').addClass('alert alert-danger');
                    $('#updateStudentCode').css('display', 'block');
                    $('#updateStudentCode .message').text('อัพเดทรหัสนักศึกษาไม่สำเร็จ!')
                }
            });
        }

        function updateName() {
            $.ajax({
                type: 'POST',
                url: '{{action('AddListControllers@updateName')}}',
                data: {
                    _token: $("[name='_token']").val(),
                    id: $("[name='id']").val(),
                    student_id: $("[name='student_id']").val(),
                    level: $("[name='level']").val(),
                    program: $("[name='program']").val(),
                    admitacadyear: $("[name='admitacadyear']").val(),
                    prefix: $("[name='prefix']").val(),
                    fname: $("[name='fname']").val(),
                    lname: $("[name='lname']").val()
                },
                success: function (data) {
                    $('.updatelist').css('display', 'block');

                }
            });
        }

        function getMessage(id) {
            $('.updatelist').css('display', 'none');
            $.ajax({
                type: 'POST',
                url: 'list-all-people/getName',
                data: {
                    _token: $("[name='_token']").val(),
                    id: id,
                },
                success: function (data) {
                    $("[name='id']").val(data.names.ID);
                    $("[name='student_id']").val(data.names.STUDENTCODE);
                    $("[name='level']").val(data.names.LEVELID);
                    $("[name='program']").val(data.names.PROGRAMID);
                    $("[name='admitacadyear']").val(data.names.ADMITACADYEAR);
                    $("[name='prefix']").val(data.names.PREFIXID);
                    $("[name='fname']").val(data.names.STUDENTNAME);
                    $("[name='lname']").val(data.names.STUDENTSURNAME);
                    $("[name='student_id']").focus();

                }
            });
        }
    </script>
@endsection
