<?php
/**
 * Created by PhpStorm.
 * User: support
 * Date: 2019-07-17
 * Time: 13:45
 */

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class CheckNameSutdentModel extends Model
{
    protected $table = 'td_activity_std';
    protected $fillable = [
        'ID',
        'IP',
        'DATE',
        'ID_STUDENT',
        'ID_ACTIVITY',
    ];
}