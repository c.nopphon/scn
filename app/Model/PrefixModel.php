<?php
/**
 * Created by PhpStorm.
 * User: OHnoBel
 * Date: 2019-07-14
 * Time: 18:20
 */

namespace App\Model;


class PrefixModel
{
    protected $table = 'tb_prefix';
    protected $fillable = [
        'PREFIXID',
        'PREFIXNAME',
    ];
}