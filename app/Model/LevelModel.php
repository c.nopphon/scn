<?php
/**
 * Created by PhpStorm.
 * User: OHnoBel
 * Date: 2019-07-14
 * Time: 18:19
 */

namespace App\Model;


class LevelModel
{
    protected $table = 'tb_faculty';
    protected $fillable = [
        'LEVELID',
        'LEVELNAME',
    ];
}