<?php
/**
 * Created by PhpStorm.
 * User: OHnoBel
 * Date: 2019-07-14
 * Time: 18:21
 */

namespace App\Model;


class ProgramModel
{
    protected $table = 'tb_prefix';
    protected $fillable = [
        'PROGRAMID',
        'FACULTYID',
        'PROGRAMNAME',
    ];
}