<?php
/**
 * Created by PhpStorm.
 * User: support
 * Date: 2019-07-16
 * Time: 09:30
 */

namespace App\Http\Controllers;

use App\Repositories\ActivityRepositories;
use App\Libraries\NotifyLineLibraries;
use App\Repositories\NotifyLineRepositories;
use Illuminate\Http\Request;
use App\Libraries\ActivityLibraries;

class ActivityControllers
{
    public $ActivityRepositories;
    public $NotifyLineLibraries;
    public $NotifyLineRepositories;
    public $ActivityLibraries;

    public function __construct(ActivityRepositories $ActivityRepositories, NotifyLineLibraries $NotifyLineLibraries, NotifyLineRepositories $NotifyLineRepositories, ActivityLibraries $ActivityLibraries)
    {
        $this->ActivityRepositories = $ActivityRepositories;
        $this->NotifyLineLibraries = $NotifyLineLibraries;
        $this->NotifyLineRepositories = $NotifyLineRepositories;
        $this->ActivityLibraries = $ActivityLibraries;
    }

    public function index()
    {
        $result = $this->ActivityRepositories->getAllActivity();
        $activityNstClass3 = $this->ActivityRepositories->getActivityByNstClass(3);
        $activityNstClass4 = $this->ActivityRepositories->getActivityByNstClass(4);
        $activityNstClass5 = $this->ActivityRepositories->getActivityByNstClass(5);
        $sumScore3 = $this->ActivityLibraries->sumScore($activityNstClass3);
        $sumScore4 = $this->ActivityLibraries->sumScore($activityNstClass4);
        $sumScore5 = $this->ActivityLibraries->sumScore($activityNstClass5);
        return view('admin.activity', [
            'listAllActivity' => $result,
            'activityNstClass3' => count($activityNstClass3),
            'activityNstClass4' => count($activityNstClass4),
            'activityNstClass5' => count($activityNstClass5),
            'sumScore3' => $sumScore3,
            'sumScore4' => $sumScore4,
            'sumScore5' => $sumScore5
        ]);
    }

    public function deleteActivity(Request $request)
    {
        $result = $this->ActivityRepositories->deleteActivityAndNameInActivity($request->get('idDelete'));
        if ($result == true) {
            return back()
                ->with('warning', 'warning')
                ->with('message', 'ลบกิจกรรมสำเร็จ!!');
        } else {
            return back()
                ->with('warning', 'danger')
                ->with('message', 'ลบกิจกรรมไม่สำเร็จ!!');
        }
    }

    public function getActivity (Request $request){
        $result = $this->ActivityRepositories->getActivityById($request->get('id'));
        return response()->json(['activity' => $result]);
    }

    public function notifyMessage(Request $request){
        $result = $this->NotifyLineRepositories->getActivityById($request->get('id'));
        $resultNotify = $this->NotifyLineLibraries->notify_message($result);
        if($resultNotify == true){
            return response()->json([
                'warning' => 'success',
                'message' => 'การแจ้งเตือน สำเร็จ !'
                ]);
        } else {
            return response()->json([
                'warning' => 'danger',
                'message' => 'การแจ้งไม่เตือน ไม่สำเร็จ !!'
            ]);
        }

    }

}