<?php
/**
 * Created by PhpStorm.
 * User: support
 * Date: 2019-07-17
 * Time: 09:52
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CheckNameSutdentRepositories;
use App\Libraries\CheckNameSutdentLibraries;
use App\Libraries\NotifyLineLibraries;

class CheckNameSutdentControllers
{
    public $CheckNameSutdentRepositories;
    public $CheckNameSutdentLibraries;
    public $notifyLineLibraries;

    public function __construct(CheckNameSutdentRepositories $CheckNameSutdentRepositories, CheckNameSutdentLibraries $CheckNameSutdentLibraries, NotifyLineLibraries $notifyLineLibraries)
    {
        $this->CheckNameSutdentRepositories = $CheckNameSutdentRepositories;
        $this->CheckNameSutdentLibraries = $CheckNameSutdentLibraries;
        $this->notifyLineLibraries = $notifyLineLibraries;
    }

    public function index($id)
    {
        $getActivity = $this->CheckNameSutdentRepositories->getActivityById($id);
        $getName = $this->CheckNameSutdentRepositories->getNameByActivityId($id);
        $countRow = count($getName);
        if ($getActivity != null) {
            return view('admin.check-name', [
                'activity' => $getActivity,
                'name' => $getName,
                'count' => $countRow,
                'warning' => null,
                'idActivity' => $id
            ]);
        } else {
            return view('admin.check-name')
                ->with('warning', 'danger')
                ->with('message', 'ไม่พบกิจกรรม');
        }
    }

    public function checkNameByStudentId(Request $request)
    {
        $getId = $this->CheckNameSutdentLibraries->checkStudentIdAndActivityId($request->get('id_activity'), $request->get('id_student'));
        if ($getId['Activity'] == null) {
            return back()
                ->with('warning', 'danger')
                ->with('message', 'บันทึกไม่สำเร็จ!! ไม่พบ กิจกรรม');

        } else if ($getId['StudentId'] == null) {
            return back()
                ->with('warning', 'danger')
                ->with('message', 'บันทึกไม่สำเร็จ!! ไม่พบ รหัสประจำตัว');
        } else {
            $checkDuplicate = $this->CheckNameSutdentRepositories->getIdStudentAndIdActivity($getId['Activity']->id, $getId['StudentId']->ID);

            if ($checkDuplicate == null) {
                $data =
                    [
                        'IP' => $request->ip(),
                        'DATE' => date('Y-m-d H:i:s'),
                        'ID_STUDENT' => $getId['StudentId']->ID,
                        'ID_ACTIVITY' => $getId['Activity']->id

                    ];

                $this->CheckNameSutdentRepositories->save($data);

                return back()
                    ->with('warning', 'success')
                    ->with('message', 'บันทึกสำเร็จ รหัสประจำตัว : ' . $request->get('id_student'));

            } else {
                return back()
                    ->with('warning', 'danger')
                    ->with('message', 'บันทึกไม่สำเร็จ!! ตรวจพบข้อมูลซ้ำ รหัส : ' . $request->get('id_student'));
            }
        }
    }

    public function checkNameViewAll($id){
        $getActivity = $this->CheckNameSutdentRepositories->getActivityById($id);
        $getName = $this->CheckNameSutdentRepositories->getAllNameByActivityId($id);
        $countRow = count($getName);
        if ($getActivity != null) {
            return view('admin.check-name-view', [
                'activity' => $getActivity,
                'name' => $getName,
                'countPeople' => $countRow,
                'warning' => null,
                'idActivity' => $id
            ]);
        } else {
            return view('admin.check-name-view')
                ->with('warning', 'danger')
                ->with('message', 'ไม่พบกิจกรรม');
        }
    }

    public function deleteNameInActivity(Request $request){
        $result = $this->CheckNameSutdentRepositories->deleteNameInActivity($request->get('idDelete'));
        if ($result) {
            return back()
                ->with('warning', 'warning')
                ->with('message', 'ลบรายชื่อสำเร็จ!!');
        } else {
            return back()
                ->with('warning', 'danger')
                ->with('message', 'ลบรายชื่อไม่สำเร็จ!!');
        }
    }
}