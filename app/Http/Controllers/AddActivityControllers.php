<?php
/**
 * Created by PhpStorm.
 * User: OHnoBel
 * Date: 2019-07-14
 * Time: 19:20
 */

namespace App\Http\Controllers;
use App\Libraries\AddActivityLibraries;
use App\Repositories\AddActivityRepositories;
use App\Libraries\CenterLibraries;
use Illuminate\Http\Request;

class AddActivityControllers
{
    public $AddActivityLibraries;
    public $AddActivityRepositories;
    public $CenterLibraries;


    public function __construct(AddActivityLibraries $AddActivityLibraries, AddActivityRepositories $AddActivityRepositories, CenterLibraries $CenterLibraries)
    {
        $this->AddActivityLibraries = $AddActivityLibraries;
        $this->AddActivityRepositories = $AddActivityRepositories;
        $this->CenterLibraries = $CenterLibraries;


    }

    public function index(){
        return view('admin.add-activity');
    }

    public function AddActivity(Request $request){
        $result = $this->AddActivityLibraries->checkNameActivity(trim($request->get('name')));

        if ($result == true) {
            $data =
                [
                    'ip' => $request->ip(),
                    'start' => $request->get('start'),
                    'end' => $request->get('end'),
                    'name_activity' => trim($request->get('name')),
                    'slug' => $this->CenterLibraries->slug($request->get('name')),
                    'nst_class' => $request->get('nst_class'),
                    'score' => $request->get('score'),
                    'remark' => $request->get('remark'),
                    'status' => '1',

                ];
            $this->AddActivityRepositories->save($data);
            return redirect('admin/activity')
                ->with('warning', 'success')
                ->with('message', 'Add Activity : ' . $data['name_activity'] . '    succeed !');

        } else {
            return back()
                ->with('warning', 'danger')
                ->with('message', 'Activity duplicate !');

        }
    }
    public function updateActivity(Request $request){
        $result = $this->AddActivityLibraries->checkDuplicateActivityByName($request->get('id'),trim($request->get('name')));
        if ($result == true) {
            $data =
                [
                    'ip' => $request->ip(),
                    'start' => $request->get('start'),
                    'end' => $request->get('end'),
                    'name_activity' => trim($request->get('name')),
                    'slug' => $this->CenterLibraries->slug($request->get('name')),
                    'nst_class' => $request->get('nst_class'),
                    'score' => $request->get('score'),
                    'remark' => $request->get('remark'),
                    'status' => '1',

                ];
            $this->AddActivityRepositories->update($request->get('id'), $data);
            return redirect('admin/activity')
                ->with('warning', 'success')
                ->with('message', 'อัพเดทกิจกรรม ' . $data["name_activity"].' สำเร็จ!');

        } else {
            return back()
                ->with('warning', 'danger')
                ->with('message', 'อัพเดทไม่กิจกรรม');

        }
    }
}