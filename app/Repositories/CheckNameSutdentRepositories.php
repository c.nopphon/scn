<?php
/**
 * Created by PhpStorm.
 * User: support
 * Date: 2019-07-17
 * Time: 09:55
 */

namespace App\Repositories;
use App\Model\CheckNameSutdentModel;
use DB;

class CheckNameSutdentRepositories
{

    public function getNameByActivityId($id){
        return DB::table('td_activity_std')
            ->leftJoin('tb_std', 'td_activity_std.ID_STUDENT', '=', 'tb_std.ID')
            ->leftJoin('tb_prefix', 'tb_std.PREFIXID', '=', 'tb_prefix.PREFIXID')
            ->select(
                "td_activity_std.ID",
                "td_activity_std.DATE",
                "tb_prefix.PREFIXNAME",
                "tb_std.STUDENTNAME",
                "tb_std.STUDENTSURNAME"

            )
            ->where('td_activity_std.ID_ACTIVITY','=',$id)
            ->orderBy('td_activity_std.DATE', 'DESC')
            ->limit(10)
            ->get();
    }
    public function getAllNameByActivityId($id){
        return DB::table('td_activity_std')
            ->leftJoin('tb_std', 'td_activity_std.ID_STUDENT', '=', 'tb_std.ID')
            ->leftJoin('tb_prefix', 'tb_std.PREFIXID', '=', 'tb_prefix.PREFIXID')
            ->select(
                "td_activity_std.ID",
                "td_activity_std.DATE",
                "tb_prefix.PREFIXNAME",
                "tb_std.STUDENTNAME",
                "tb_std.STUDENTSURNAME"

            )
            ->where('td_activity_std.ID_ACTIVITY','=',$id)
            ->orderBy('td_activity_std.DATE', 'DESC')
            ->get();
    }
    public function save($data){
        $members = new CheckNameSutdentModel($data);
        $members->save();
    }

    public function getActivityById($id)
    {
        return DB::table('tb_activity')
            ->where('id','=',$id)
            ->get()->first();
    }

    public function getIdActivityById($id)
    {
        return DB::table('tb_activity')
            ->where('id','=',$id)
            ->select('id')
            ->get()->first();
    }
    public function getIdStudentById($id_student){
        return DB::table('tb_std')
            ->where('STUDENTCODE','=',$id_student)
            ->select('ID')
            ->get()->first();
    }
    public function getIdStudentAndIdActivity($id_activity, $id_student){
        return DB::table('td_activity_std')
            ->where('ID_ACTIVITY','=',$id_activity)
            ->where('ID_STUDENT','=',$id_student)
            ->select('ID')
            ->get()->first();
    }
    public function deleteNameInActivity($id){
        return DB::table('td_activity_std')->where('ID', $id)->delete();
    }
}