<?php
/**
 * Created by PhpStorm.
 * User: support
 * Date: 2019-07-18
 * Time: 20:09
 */

namespace App\Repositories;
use DB;


class NotifyLineRepositories
{
    public function getActivityById($id)
    {
        return DB::table('tb_activity')
            ->where('status','=','1')
            ->where('id','=',$id)
            ->get()->first();
    }
}