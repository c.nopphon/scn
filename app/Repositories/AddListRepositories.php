<?php
/**
 * Created by PhpStorm.
 * User: OHnoBel
 * Date: 2019-07-14
 * Time: 18:27
 */

namespace App\Repositories;
use App\Model\StudentModel;
use DB;

class AddListRepositories
{
    public function getAllLevel()
    {
        return DB::table('tb_level')
            ->orderBy('LEVELID', 'desc')
            ->get();
    }

    public function getAllProgram()
    {
        return DB::table('tb_program')
            ->orderBy('PROGRAMNAME', 'ASC')
            ->get();
    }

    public function getAllPrefix()
    {
        return DB::table('tb_prefix')
            ->orderBy('PREFIXNAME', 'ASC')
            ->get();
    }

    public function getIdStudent($id)
    {
        return DB::table('tb_std')
            ->where('STUDENTCODE','=',$id)
            ->get()->first();
    }

    public function save($data){
        $result = new StudentModel($data);
        $result->save();
    }

    public function update($id,$data)
    {
        return DB::table('tb_std')
            ->where('ID', $id)
            ->update($data);
    }
}