<?php
/**
 * Created by PhpStorm.
 * User: support
 * Date: 2019-07-15
 * Time: 16:33
 */

namespace App\Repositories;
use App\Model\ActivityModel;
use DB;


class AddActivityRepositories
{
    public function getNameActivity($name)
    {
        return $name = DB::table('tb_activity')->where([
            ['slug', '=', $name],
        ])->get()->first();
    }

    public function save($data)
    {
        $members = new ActivityModel($data);
        $members->save();
    }

    public function update($id, $data)
    {
        return DB::table('tb_activity')
            ->where('id', $id)
            ->update($data);
    }
}

