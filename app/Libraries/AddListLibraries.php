<?php
/**
 * Created by PhpStorm.
 * User: support
 * Date: 2019-07-18
 * Time: 14:18
 */

namespace App\Libraries;
use App\Repositories\AddListRepositories;

class AddListLibraries
{
    public function checkDuplicateStudentId($id, $student_id)
    {
       $getIdStundet = new AddListRepositories();
       $result = $getIdStundet->getIdStudent($student_id);
        if($result == null){
            return true;
        } else if($id == $result->ID){
            return true;
        } else {
            return false;
        }
    }
}