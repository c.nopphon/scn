<?php
/**
 * Created by PhpStorm.
 * User: support
 * Date: 2019-07-15
 * Time: 16:36
 */

namespace App\Libraries;


class CenterLibraries
{
    public function slug($text){
        $text = trim($text);
        $text = strtolower($text);
        $text = str_replace(" ", "-", $text);
        return $text;

    }
}