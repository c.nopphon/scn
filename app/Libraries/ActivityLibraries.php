<?php
/**
 * Created by PhpStorm.
 * User: support
 * Date: 2019-07-22
 * Time: 20:19
 */

namespace App\Libraries;


class ActivityLibraries
{
        public function sumScore($data){
            $count = 0;
            foreach ($data as $val){
                    $count += $val->score;
            }
            return $count;
        }
}