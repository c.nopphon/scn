<?php
/**
 * Created by PhpStorm.
 * User: support
 * Date: 2019-07-15
 * Time: 16:33
 */

namespace App\Libraries;
use App\Repositories\AddActivityRepositories;
use App\Libraries\CenterLibraries;


class AddActivityLibraries
{
    public function checkNameActivity($name)
    {
        $slug = new CenterLibraries();
        $resultSlug = $slug->slug($name);

        $AddActivityRepository = new AddActivityRepositories();
        $result = $AddActivityRepository->getNameActivity($resultSlug);

        if (!$result) {
            return true;
        } else {
            return false;
        }


    }

    public function checkDuplicateActivityByName($id, $name){
        $slug = new CenterLibraries();
        $resultSlug = $slug->slug($name);

        $AddActivityRepository = new AddActivityRepositories();
        $result = $AddActivityRepository->getNameActivity($resultSlug);
        if($result == null){
            return true;
        } else if($id == $result->id){
            return true;
        } else {
            return false;
        }

    }

}